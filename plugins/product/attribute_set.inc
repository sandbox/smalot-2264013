<?php

/**
 * @file
 * Magento mapping product api.
 */

/**
 * Implements hook_form().
 */
function magento_mapping_product_attribute_set($form, $form_state) {

  $header = array(
    'name'        => t('Name'),
    'code'        => t('Code'),
    'last_synced' => t('Last synced'),
  );

  $rows = $default_values = array();
  $sets = magento_mapping_product_attribute_set_list();
  foreach ($sets as $set) {
    $code_field     = array(
      '#type'          => 'textfield',
      '#value'         => $set->code,
      '#title'         => 'Comment',
      '#title_display' => 'invisible',
      '#name'          => 'code[' . $set->id . ']',
      '#attributes'    => array(
        'size' => 20,
      ),
    );
    $rows[$set->id] = array(
      'name'        => $set->name,
      'code'        => array('data' => $code_field),
      'last_synced' => (NULL === $set->last_synced ? '' : format_date($set->last_synced)),
    );
    if ($set->selected) {
      $default_values[$set->id] = $set->id;
    }
  }

  $form['attribute']['table'] = array(
    '#type'          => 'tableselect',
    '#header'        => $header,
    '#options'       => $rows,
    '#empty'         => t('No content available.'),
    '#suffix'        => '',
    '#multiple'      => TRUE,
    '#js_select'     => TRUE,
    '#default_value' => $default_values,
  );

  $form['form-actions'] = array(
    '#prefix' => '<div class="form-actions form-wrapper">',
    '#suffix' => '</div>',
  );

  $form['form-actions']['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Update'),
  );

  $form['form-actions']['reload'] = array(
    '#type'  => 'submit',
    '#value' => t('Reload from Magento'),
  );

  $form['form-actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Sync product variants'),
  );

  $legend = '<p><strong>Update:</strong> save selections and code values,<br/>' .
    '<strong>Reload from Magento:</strong> retrieve all information from Magento and store it into database,<br/>' .
    '<strong>Sync product variants:</strong> create/update product variants based on local information.' .
    '</p>';

  $form['legend'] = array(
    '#markup' => $legend,
  );

  // Hack to retrieve code fields.
  $form['code'] = array('#type' => 'value');

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function magento_mapping_product_attribute_set_submit($form, $form_state) {

  if ($form_state['values']['op'] == $form_state['values']['reload']) {
    magento_mapping_product_attribute_set_reload();

    drupal_set_message(t('Resynced from Magento'));
  }
  elseif ($form_state['values']['op'] == $form_state['values']['update']) {
    $ids = array_filter($form_state['values']['table']);

    // Reset values.
    db_update('magento_attribute_set')
      ->fields(array('selected' => 0))
      ->execute();

    // Set new values.
    if ($ids) {
      db_update('magento_attribute_set')
        ->fields(array('selected' => 1))
        ->condition('id', $ids, 'in')
        ->execute();
    }

    $codes = (is_array($form_state['values']['code']) ? $form_state['values']['code'] : array());

    foreach ($codes as $id => $code) {
      $code = magento_mapping_sluggify($code);

      db_update('magento_attribute_set')
        ->fields(array('code' => $code))
        ->condition('id', $id)
        ->execute();
    }

    drupal_set_message(t('Updated'));
  }
  elseif ($form_state['values']['op'] == $form_state['values']['submit']) {

    drupal_set_message(t('Synced'));
  }
}

/**
 * Returns the list of attributes sets already synced.
 *
 * @return mixed
 *   List of attribute sets.
 */
function magento_mapping_product_attribute_set_list() {
  $results = db_select('magento_attribute_set', 's')
    ->fields('s')
    ->orderBy('name')
    ->execute();

  return $results->fetchAllAssoc('id');
}

/**
 * Reload all attribute sets.
 *
 * @param string $code
 *   Magento connection code.
 *
 * @return bool
 *   Return status.
 */
function magento_mapping_product_attribute_set_reload($code = 'default') {
  $magento = magento_api_provider_get($code);

  $attribute_set_manager = new \Smalot\Magento\Catalog\ProductAttributeSet($magento);
  $attribute_sets        = $attribute_set_manager->getList()->execute();

  if ($attribute_sets) {
    $current_list = magento_mapping_product_attribute_set_list();

    foreach ($attribute_sets as $attribute_set) {
      if (!isset($current_list[$attribute_set['set_id']])) {
        $code = magento_mapping_sluggify($attribute_set['name']);

        db_insert('magento_attribute_set')
          ->fields(
            array(
              'set_id'  => $attribute_set['set_id'],
              'name'    => $attribute_set['name'],
              'code'    => $code,
              'created' => REQUEST_TIME,
              'updated' => REQUEST_TIME,
            )
          )
          ->execute();
      }
      else {

      }
    }

    return TRUE;
  }

  return FALSE;
}
