<?php

/**
 * @file
 * Magento mapping product api.
 */

/**
 * Implements hook_form().
 */
function magento_mapping_product_attribute($form, $form_state) {

  $header = array(
    'name'          => t('Name'),
    'type'          => t('Type'),
    'field'         => t('Field'),
    'attribute_set' => t('Attribute sets'),
    'last_synced'   => t('Last synced'),
  );

  $rows       = $default_values = array();
  $attributes = magento_mapping_product_attribute_list();
  foreach ($attributes as $attribute) {
    $field_name = array(
      '#type'          => 'textfield',
      '#value'         => $attribute->field,
      '#title'         => t('Comment'),
      '#title_display' => 'invisible',
      '#name'          => 'field[' . $attribute->id . ']',
      '#attributes'    => array(
        'size' => 20,
      ),
    );

    $rows[$attribute->id] = array(
      'name'          => $attribute->name,
      'type'          => $attribute->type,
      'field'         => array('data' => $field_name),
      'attribute_set' => $attribute->set_name,
      'last_synced'   => (NULL === $attribute->last_synced ? '' : format_date($attribute->last_synced)),
    );

    if ($attribute->selected) {
      $default_values[$attribute->id] = $attribute->id;
    }
  }

  $form['attribute']['table'] = array(
    '#type'          => 'tableselect',
    '#header'        => $header,
    '#options'       => $rows,
    '#empty'         => t('No content available.'),
    '#suffix'        => '',
    '#multiple'      => TRUE,
    '#js_select'     => TRUE,
    '#default_value' => $default_values,
  );

  $form['form-actions'] = array(
    '#prefix' => '<div class="form-actions form-wrapper">',
    '#suffix' => '</div>',
  );

  $form['form-actions']['update'] = array(
    '#type'  => 'submit',
    '#value' => t('Update'),
  );

  $form['form-actions']['reload'] = array(
    '#type'  => 'submit',
    '#value' => t('Reload from Magento'),
  );

  $form['form-actions']['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Sync product fields'),
  );

  $legend = t(
    '<p><strong>Update:</strong> save selections and code values,<br/>' .
    '<strong>Reload from Magento:</strong> retrieve all information from Magento and store it into database,<br/>' .
    '<strong>Sync product fields:</strong> create/update product fields based on local information.' .
    '</p>'
  );

  $form['legend'] = array(
    '#markup' => $legend,
  );

  // Hack to retrieve code fields.
  $form['field'] = array('#type' => 'value');

  return $form;
}

/**
 * Implements hook_form_submit().
 */
function magento_mapping_product_attribute_submit($form, $form_state) {
  if ($form_state['values']['op'] == $form_state['values']['reload']) {
    magento_mapping_product_attribute_reload();

    drupal_set_message(t('Resynced from Magento'));
  }
  elseif ($form_state['values']['op'] == $form_state['values']['update']) {
    $ids = array_filter($form_state['values']['table']);

    // Reset values.
    db_update('magento_attribute')
      ->fields(array('selected' => 0))
      ->execute();

    // Set new values.
    if ($ids) {
      db_update('magento_attribute')
        ->fields(array('selected' => 1))
        ->condition('id', $ids, 'in')
        ->execute();
    }

    $fields = (is_array($form_state['values']['field']) ? array_filter($form_state['values']['field']) : array());

    foreach ($fields as $id => $field) {
      $field = magento_mapping_sluggify($field);

      db_update('magento_attribute')
        ->fields(array('field' => $field))
        ->condition('id', $id)
        ->execute();
    }

    drupal_set_message(t('Updated'));
  }
  elseif ($form_state['values']['op'] == $form_state['values']['submit']) {

    magento_mapping_product_attribute_sync();

    drupal_set_message(t('Synced'));
  }
}

/**
 * Returns the list of attributes sets already synced.
 *
 * @return mixed
 *   List of attribute sets.
 */
function magento_mapping_product_attribute_list() {

  $query   = "SELECT a.*, GROUP_CONCAT(s.name ORDER BY s.name ASC SEPARATOR ', ') as set_name
            FROM {magento_attribute} a
            LEFT JOIN {magento_attribute_link_set} ls ON ls.attribute_id = a.attribute_id
            LEFT JOIN {magento_attribute_set} s       ON s.set_id = ls.set_id
            GROUP BY a.id
            ORDER BY a.code";
  $results = db_query($query)->fetchAllAssoc('id');

  foreach ($results as &$result) {
    $result->info = @unserialize($result->info);
  }

  return $results;
}

/**
 * Reload all attributes.
 *
 * @param string $code
 *   Magento connection code.
 *
 * @return bool
 *   Return status.
 */
function magento_mapping_product_attribute_reload($code = 'default') {
  $magento = magento_api_provider_get($code);
  $queue   = new \Smalot\Magento\MultiCallQueue($magento);

  // Load attribute sets list.
  $attribute_set_manager = new \Smalot\Magento\Catalog\ProductAttributeSet($magento);
  $attribute_sets        = $attribute_set_manager->getList()->execute();

  // List all attribute by attribute set.
  $attribute_manager = new \Smalot\Magento\Catalog\ProductAttribute($magento);
  $set_ids           = array();
  foreach ($attribute_sets as $attribute_set) {
    $set_ids[] = $attribute_set['set_id'];
    $queue->addAction($attribute_manager->getList($attribute_set['set_id']));
  }
  $attributes_by_set = array_combine($set_ids, $queue->execute());

  // Load all attribute info.
  $queue->flush();
  $attributes_full = array();
  foreach ($attributes_by_set as $attribute_set_id => $attributes) {
    foreach ($attributes as $attribute) {
      if (!in_array($attribute['attribute_id'], array_keys($attributes_full))) {
        $attributes_full[$attribute['attribute_id']] = $attribute;
        $queue->addAction($attribute_manager->getInfo($attribute['attribute_id']));
      }

      $attributes_full[$attribute['attribute_id']]['set_ids'][$attribute_set_id] = $attribute_set_id;
    }
  }

  $results = $queue->execute();
  foreach ($results as $result) {
    $attributes_full[$result['attribute_id']]['info'] = $result;
  }

  // Refresh database.
  if ($attributes_full) {
    $current_list  = magento_mapping_product_attribute_list();
    $attribute_ids = array();
    foreach ($current_list as $current) {
      $attribute_ids[$current->attribute_id] = $current->id;
    }

    foreach ($attributes_full as $attribute) {
      $name = isset($attribute['info']['frontend_label'][0]['label']) ? $attribute['info']['frontend_label'][0]['label'] : '';
      if (!trim($name)) {
        $name = ucwords(str_replace('_', ' ', $attribute['code']));
      }

      if (!in_array($attribute['attribute_id'], array_keys($attribute_ids))) {
        $field = magento_mapping_sluggify($attribute['code']);

        db_insert('magento_attribute')
          ->fields(
            array(
              'attribute_id' => $attribute['attribute_id'],
              'name'         => $name,
              'code'         => $attribute['code'],
              'type'         => (trim($attribute['type']) ? $attribute['type'] : 'text'),
              'field'        => $field,
              'created'      => REQUEST_TIME,
              'updated'      => REQUEST_TIME,
              'info'         => serialize($attribute['info']),
            )
          )
          ->execute();
      }
      else {
        db_update('magento_attribute')
          ->fields(
            array(
              'attribute_id' => $attribute['attribute_id'],
              'name'         => $name,
              'code'         => $attribute['code'],
              'type'         => (trim($attribute['type']) ? $attribute['type'] : 'text'),
              'updated'      => REQUEST_TIME,
              'info'         => serialize($attribute['info']),
            )
          )
          ->condition('attribute_id', $attribute['attribute_id'])
          ->execute();
      }

      db_delete('magento_attribute_link_set')
        ->condition('attribute_id', $attribute['attribute_id'])
        ->execute();

      $query = db_insert('magento_attribute_link_set')
        ->fields(array('attribute_id', 'set_id'));
      foreach ($attribute['set_ids'] as $set_id) {
        $query->values(
          array(
            $attribute['attribute_id'],
            $set_id,
          )
        );
      }
      $query->execute();
    }

    return TRUE;
  }

  return FALSE;
}

/**
 * Sync fields.
 *
 * @param string $code
 *   Magento connection code.
 *
 * @return bool
 *   Return status.
 */
function magento_mapping_product_attribute_sync($code = 'default') {

  $attributes = magento_mapping_product_attribute_list();
  $options    = _magento_mapping_product_attribute_options($code);

  foreach ($attributes as $attribute) {
    // Handle only selected attributes.
    if (!$attribute->selected) {
      continue;
    }

    // Allow to check if field has been updated.
    $checksum = md5(json_encode($attribute->info));

    // Field name is limited to 32 chars length.
    $field_name = substr('magento_' . $attribute->field, 0, 32);

    // Load current field if exists.
    if ($field = field_info_field($field_name)) {
      // Field is up to date ?
      if (isset($field['settings']['checksum']) && $field['settings']['checksum'] == $checksum) {
        continue;
      }
    }

    switch ($attribute->type) {
      case 'price':
      case 'weight':
        $field_settings = array('type' => 'number_decimal');
        break;

      case 'date':
        $field_settings = array('type' => 'date');
        break;

      case 'media_image':
      case 'text':
        $field_settings = array('type' => 'text');
        break;

      case 'textarea':
        $field_settings = array('type' => 'text_long');
        break;

      case 'boolean':
      case 'select':
      case 'multiselect':
        // Update taxonomy vocabulary.
        $vocabulary = _magento_mapping_product_attribute_taxonomy(
          $attribute->name,
          $attribute->field,
          $options[$attribute->attribute_id]
        );

        $field_settings = array(
          'type'        => 'taxonomy_term_reference',
          'cardinality' => ($attribute->type == 'multiselect' ? FIELD_CARDINALITY_UNLIMITED : 1),
          'settings'    => array(
            'allowed_values' => array(
              array(
                'vocabulary' => $vocabulary->vid,
              )
            ),
          ),
        );

        break;

      default:
        die('Not supported attribute type: ' . $attribute->type);
    }

    // Set generic values.
    $field_settings += array(
      'field_name' => $field_name,
      'settings'   => array(),
    );

    // Debug information.
    $field_settings['settings'] += array(
      'code'        => $attribute->code,
      'entity'      => 'magento',
      'checksum'    => $checksum,
      'last_update' => REQUEST_TIME,
    );

    if (isset($field) && $field['type'] != $field_settings['type']) {
      drupal_set_message(
        t('@field_name was skipped due to altered field type', array('@field_name' => $field_name)),
        'warning'
      );
    }
    else {
      if (!$field) {
        field_create_field($field_settings);
        drupal_set_message(t('@field_name was created', array('@field_name' => $field_name)));
      }
      else {
        field_update_field($field_settings);
        drupal_set_message(t('@field_name was updated', array('@field_name' => $field_name)));
      }

      db_update('magento_attribute')
        ->fields(array('last_synced' => REQUEST_TIME))
        ->condition('id', $attribute->id)
        ->execute();
    }
  }

  return TRUE;
}

/**
 * List of options by field.
 *
 * @param string $code
 *   Connection code.
 *
 * @return array
 *   List of options.
 */
function _magento_mapping_product_attribute_options($code) {
  $attributes = db_select('magento_attribute', 'a')
    ->fields('a')
    ->condition('type', 'select')
    ->execute()
    ->fetchAllAssoc('attribute_id');

  $ids               = array();
  $magento           = magento_api_provider_get($code);
  $queue             = new \Smalot\Magento\MultiCallQueue($magento);
  $attribute_manager = new \Smalot\Magento\Catalog\ProductAttribute($magento);

  foreach ($attributes as $attribute_id => $attribute) {
    $ids[]  = $attribute_id;
    $action = $attribute_manager->getOptions($attribute_id);
    $queue->addAction($action);
  }

  $results = $queue->execute();
  $results = array_combine($ids, $results);

  return $results;
}

/**
 * Create/update vocabulary.
 *
 * @param string $name
 *   Name.
 * @param string $machine_name
 *   Machine name.
 * @param array  $options
 *   Term values.
 *
 * @return stdClass
 *   Vocabulary.
 */
function _magento_mapping_product_attribute_taxonomy($name, $machine_name, $options) {
  // Load or create vocabulary if needed.
  if (!$vocabulary = taxonomy_vocabulary_machine_name_load($machine_name)) {
    $vocabulary               = new stdClass();
    $vocabulary->name         = 'Product: ' . ucwords(str_replace('_', ' ', $name));
    $vocabulary->machine_name = $machine_name;
    taxonomy_vocabulary_save($vocabulary);

    $terms = array();
  }
  else {
    // Load current terms.
    $terms = taxonomy_term_load_multiple(array(), array('vid' => $vocabulary->vid));
  }

  // Create missing terms.
  foreach ($options as $option) {
    if ($option['value']) {
      $found = FALSE;
      // To avoid unnecessary warning.
      $term = new stdClass();

      foreach ($terms as $term) {
        if ($term->name == $option['label']) {
          $found = TRUE;
          break;
        }
      }

      if (!$found) {
        $term = new stdClass();
      }

      $term->vid  = $vocabulary->vid;
      $term->name = $option['label'];
      taxonomy_term_save($term);
    }
  }

  return $vocabulary;
}
