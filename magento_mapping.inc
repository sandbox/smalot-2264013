<?php

/**
 * @file
 * Magento mapping core api.
 */

/**
 * Create slug from text.
 *
 * @param string $text
 *   Text.
 * @param string $replace
 *   Text used in replacement.
 *
 * @return string
 *   Slug.
 */
function magento_mapping_sluggify($text, $replace = '_') {
  $text = str_replace(array('/', ' ', '-', '_', '.'), $replace, $text);
  $text = strtolower(htmlentities(utf8_decode($text)));

  // Removes special chars.
  $text = preg_replace('/\%[0-9]{2}/', '', $text);

  // Removes accents.
  $accents = '/&([A-Za-z]{1,2})(tilde|grave|acute|circ|cedil|uml|lig);/';
  $text    = preg_replace($accents, '$1', $text);
  
  // Final clean.
  $text = preg_replace('/[^A-Z0-9' . preg_quote($replace) . ']/i', '', $text);

  return $text;
}
